const generalEndFundInvalidate = require('./generalEndFundInvalidate');
const generalInvalidation = require('./generalInvalidation');

export const jobs = {
  generalEndFundInvalidate,
  generalInvalidation
};
