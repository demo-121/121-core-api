const aDao = require('../cbDao/agent');
const dao = require('../cbDaoFactory').create();
const _ = require('lodash');
const logger = global.logger || console;
const {
  callApiByGet
} = require('../utils/RemoteUtils');
const moment = require('moment');

module.exports.getAllCustomer = function () {
  return new Promise(resolve => {
    dao.getViewRange('main', 'contacts', '', '', null, (pList = {}) => {
      resolve(_.compact(_.map(pList.rows, 'id')));
    });
  });
};


module.exports.getAllCustomerWithFundNHAF = function () {
  return new Promise(resolve => {
    dao.getViewRangeNHAF('invalidateViews', 'quotationsByNHAFFund', '', '', null, (pList = {}) => {
      let result = [];
      _.each(pList && pList.rows, row => {
        if (row && row.value && row.value.cids) {
          result = _.union(result, row.value.cids);
        }
      });
      resolve(result);
    });
  });
};


module.exports.getAllCustomerWithMutipleBaseProductCode = function () {
  return new Promise(resolve => {
    dao.getViewRangeNHAF('invalidateViews', 'quotationsByMutipleBaseProductCode', '', '', null, (pList = {}) => {
      let result = [];
      _.each(pList && pList.rows, row => {
        if (row && row.value && row.value.clientId && row.value.bundleId) {
          result.push(row.value);
        }
      });
      resolve(result);
    });
  });
};


module.exports.getAllValidBundleId = function () {
  return new Promise(resolve => {
    dao.getViewRange('invalidateViews', 'validBundleInClient', '', '', null, (bundles = {}) => {
      resolve(_.compact(_.map(bundles.rows, row => row && row.value && row.value.validBundleId)));
    });
  });
};


module.exports.getImpactedClientIds = function (bundleIds, handleCovCode, compCode = '01') {
  let promises = [];
  _.each(handleCovCode, (isHandle, covCode) => {
    if (isHandle) {
      promises.push(
        new Promise(resolve => {
          let keys = _.map([covCode], code => '["' + compCode + '","' + code + '"]');
          dao.getViewByKeys('invalidateViews', 'quotationsByBaseProductCode', keys, null).then((result) => {
            let clientIds = [];
            if (result) {
              _.each(result.rows, (row) => {
                let quotation = row.value || {};
                if (quotation.clientId && quotation.bundleId && bundleIds.indexOf(quotation.bundleId) > -1) {
                  clientIds = _.union(clientIds, [quotation.clientId]);
                }
              });
            }
            resolve(clientIds);
          });
        })
      );
    }
  });

  return Promise.all(promises).then(clientIdsArray => {
    let results = [];
    _.forEach(clientIdsArray, clientIds => {
      results = _.union(results, clientIds);
    });
    return results;
  });
};

module.exports.getAllAgents = function (impactAgents = []) {
  return new Promise(resolve => {
    dao.getViewRange('main', 'agents', '', '', null, (pList = {}) => {
      resolve(_.map(pList.rows, data => {
        return {
          id: _.get(data, 'value.profileId'),
          agentCode: _.get(data, 'value.agentCode')
        };
      }));
    });
  });
};

module.exports.getAllAgentRelatedDocumentIds = function (compCode, agentCodes) {
  return new Promise(resolve => {
    let keys = _.map([agentCodes], agentCode => '["' + compCode + '","' + agentCode + '"]');
    dao.getViewByKeys('dataSync', 'agentDocuments', keys, null).then((pList = {}) => {
      resolve(_.map(pList.rows, data => {
        return data && data.id;
      }));
    });
  });
};

module.exports.getFilterAgent = function (impactAgents = []) {
  return new Promise(resolve => {
    dao.getViewRange('main', 'agents', '', '', null, (pList = {}) => {
      const result = _.filter(pList.rows, row => impactAgents.indexOf(row.key[1]) > -1);
      resolve(_.map(result, 'value.profileId'));
    });
  });
};

module.exports.insertNoticeToAgent = function (agentId, id, message, messageGroup, groupPriority) {
  return new Promise(resolve => {
    dao.getDoc("inflightJobList", doc => {
      const newMessages = _.cloneDeep(doc.messages);
      id.forEach((idValue, index) => {
        if (!newMessages.find(item => item.messageId === idValue)) {
          newMessages.push({
            messageId: idValue,
            message: message[index]
          });
        }
      });

      dao.updDoc("inflightJobList", Object.assign({}, doc, {messages:newMessages}), resolve);
    });
  }).catch(error => {
    logger.error(`Jobs :: InflightInvalidateJob :: ERROR caught at insertNoticeToAgent ${error}`);
    throw Error(`Fail to insert notification to agent ${agentId}`);
  });
};

module.exports.checkIsJobExecutable = function (runTime, sysParam) {
  return new Promise((resolve, reject) => {
    let fromTime = _.get(sysParam, 'fromTime');
    let toTime = _.get(sysParam, 'toTime');
    if (fromTime && toTime) {
      let runDateTime = moment(runTime);
      // The range of fromTime and toTime needs to include the scheduleCron Time
      let fromDateTime = moment(fromTime, 'YYYY-MM-DD HH:mm:ss');
      let toDateTime = moment(toTime, 'YYYY-MM-DD HH:mm:ss');
      //call API to check if this node server can perform Invalidation or not
      if (runDateTime >= fromDateTime && runDateTime <= toDateTime) {
        callApiByGet('/fairBiJobRunner', (result) => {
          resolve(_.get(result, 'success'));
        });
      } else {
        resolve(false);
      }
    } else {
      reject(new Error(`Fail to get fromTime="${fromTime}" toTime="${toTime}" in ${sysParam}`));
    }
  });
};

module.exports.addTrxLogs = function (runTime, trxLogs, errLogs, jobDetailsDocKey) {
  let nowString = moment().toDate().toISOString();
  let runTimeLong = runTime.getTime();
  let runTimeString = runTime.toISOString();
  return new Promise((resolve) => {
    dao.getDoc(jobDetailsDocKey, (doc) => {
      if (doc && !doc.error) {
        let trxData = _.get(doc, `log.${runTimeLong}`);
        if (trxData) {
          let currData = _.cloneDeep(trxData);
          trxData.transaction = _.has(currData, 'transaction') ? _.concat([], currData.transaction, trxLogs) : trxLogs;
          if (!_.isEmpty(errLogs)) {
            trxData.error = _.has(currData, 'error') ? _.concat([], currData.error, errLogs) : errLogs;
          }
        } else {
          trxData = Object.assign({}, {
              transaction: trxLogs
            },
            !_.isEmpty(errLogs) ? {
              error: errLogs
            } : {}
          );
        }
        trxData.completeTime = nowString;
        trxData.runTime = runTimeString;

        doc.log[runTimeLong] = trxData;

        logger.log(`Jobs :: DataPatchJob_DataSync :: update doc ${jobDetailsDocKey} - runTime=${runTimeString} completedTime=${nowString}`);
        dao.updDoc(jobDetailsDocKey, doc, (result) => {
          doc._rev = result.rev;
          resolve(doc);
        });
      } else if (doc && doc.error === 'not_found') {
        let newDoc = {
          log: {}
        };

        newDoc.log[runTimeLong] = Object.assign({}, {
            runTime: runTimeString,
            completeTime: nowString,
            transaction: trxLogs
          },
          !_.isEmpty(errLogs) ? {
            error: errLogs
          } : {}
        );

        logger.log(`Jobs :: DataPatchJob_DataSync :: new doc ${jobDetailsDocKey} - runTime=${runTimeString} completedTime=${nowString}`);
        dao.updDoc(jobDetailsDocKey, newDoc, (result) => {
          newDoc._rev = result.rev;
          resolve(newDoc);
        });
      } else {
        logger.log(`Jobs :: DataPatchJob_DataSync :: cannot find job details - runTime=${runTimeString} completedTime=${nowString}`);
        resolve(false);
      }
    });
  });
};
