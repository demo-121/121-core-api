const _ = require('lodash');
const logger = global.logger || console;
const moment = require('moment');

const invalidateShieldRiderRule = (invalidateParams) => {
  let {baseProductCode,quotation, ruleParams} = invalidateParams;
  let {
    covCodes
  } = ruleParams;
  if (baseProductCode !== 'ASIM' || !quotation || !covCodes) {
    return false;
  }
  let isShieldWithRiders = false;
  let {
    insureds
  } = quotation;
  if (_.isPlainObject(insureds)) {
    _.forEach(insureds, function (quot, cids) {
      if (quot) {
        let {
          plans
        } = quot;
        _.forEach(plans, plan => {
          let {
            covCode
          } = plan;
          if (covCodes.indexOf(covCode) > -1) {
            isShieldWithRiders = true;
            return false;
          }
        });
        if (isShieldWithRiders) {
          return false;
        }
      }
    });
  }
  return isShieldWithRiders;
};


const invalidateProductsRule = (invalidateParams) => {
  let {baseProductCode, ruleParams} = invalidateParams;
  let {
    productCodes
  } = ruleParams;
  if (productCodes.indexOf(baseProductCode) > -1) {
    return true;
  }
  return false;
};

const invalidateByCreateDateRule = (invalidateParams) => {
  let {createDate, ruleParams} = invalidateParams;
  if (moment(createDate).isValid()) {
    let {toDate} = ruleParams;
    let diff = moment(createDate).diff(toDate);
    //createDate before toDate
    if (diff < 0) {
      return true;
    }
  }
  return false;
};

const allRulesRunner = {
  invalidateShieldRiderRule: invalidateShieldRiderRule,
  invalidateProductsRule: invalidateProductsRule,
  invalidateByCreateDateRule: invalidateByCreateDateRule
};

module.exports.mainRulesHandler = (inflightInputData, jobParams) => {
  let {bundleApplications, docs} = inflightInputData;
  let {ruleIds, rulesParamsMapping} = jobParams;
  let allRulesResults = {};
  let docIds2RulesResult = {};
  let rules2docIdsResult = {};
  if (bundleApplications && _.isArray(bundleApplications)){
    _.forEach(bundleApplications, app => {
      logger.log(app);
      let isApplication = _.has(app, 'applicationDocId');
      let docId = isApplication ? _.get(app, 'applicationDocId', '') : _.get(app, 'quotationDocId', '');
      if (docId){
        let data = _.find(docs, result => {return result.docId === docId && result.isApplication === isApplication;});
        let passRuleResults = {};

        if (data) {
          let doc = data.doc;
          let baseProductCode = isApplication ? _.get(doc, 'quotation.baseProductCode', '') : _.get(doc, 'baseProductCode', '');
          let quotation = isApplication ? _.get(doc, 'quotation', null) : doc;
          let {createDate} = quotation;

          _.forEach(ruleIds, ruleId => {
            if (ruleId) {
              let ruleParams = rulesParamsMapping[ruleId];
              let {functId} = ruleParams;
              let isRulePass = false;

              //check wheteher pass in each rule
              let invalidateFunct = allRulesRunner[functId];
              let invalidateParams = {baseProductCode,quotation, createDate, ruleParams};
              isRulePass = invalidateFunct(invalidateParams);

              passRuleResults[ruleId] = isRulePass;

              if (!_.hasIn(rules2docIdsResult,ruleId)){
                rules2docIdsResult[ruleId] = [];
              }
              if (isRulePass){
                rules2docIdsResult[ruleId].push(docId);
              }
            }
          });
        }
        docIds2RulesResult[docId] = passRuleResults;
      }
    });
    allRulesResults.rules2docIdsResult = rules2docIdsResult;
    allRulesResults.docIds2RulesResult = docIds2RulesResult;
  }

  return allRulesResults;
};
