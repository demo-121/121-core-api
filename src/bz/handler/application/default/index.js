const sign = require('./signature');


module.exports.getSignatureInitUrl = sign.getSignatureInitUrl;
module.exports.getSignatureUpdatedUrl = sign.getSignatureUpdatedUrl;
