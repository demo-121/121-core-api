const _ = require('lodash');
const moment = require('moment');
const dao = require('./cbDaoFactory').create();
const bundleDao = require('./cbDao/bundle/index');
const appDao = require('./cbDao/application');
const logger = global.logger || console;

/**
 * @description this file implemented the batch job that is running in mobile only
 */

const sysParameter = "sysParameter";
const BATCH_SIGN_EXPIRY_TRX = "BATCH_SIGN_EXPIRY_TRX";

const addTrxLogs = (agentCode, runTime, docType, logs) => {
  return new Promise(resolve => {
    const runTimeString = runTime.toISOString();
    const docId = `${docType}_${agentCode}`;
    const updateLogs = (docId, newDoc) => {
      dao.updDoc(docId, newDoc, result => {
        newDoc._rev = result.rev;
        resolve(newDoc);
      });
    };

    dao.getDoc(docId, (doc) => {
      if (doc && !doc.error) {
        const newDoc = _.cloneDeep(doc);
        newDoc.rows = doc.rows.concat(logs);

        logger.log(`INFO: InvaildationHandler - addTrxLogs :: update doc ${docType} - runTime=${runTimeString}`);
        updateLogs(docId, newDoc);
      } else if (doc && doc.error === 'not_found') {
        const newDoc = {
          rows: logs,
          type: docType
        };
        logger.log(`INFO: InvaildationHandler - addTrxLogs :: new doc ${docType} - runTime=${runTimeString}`);
        updateLogs(docId, newDoc);
      } else {
        logger.error(`ERROR: InvaildationHandler - addTrxLogs :: error=${doc.error} runTime=${runTimeString}`);
        resolve(false);
      }
    });
  });
};

const signatureExpiry = (data, session, cb) => {
  const { cid, applicationId } = data;
  const now = moment();
  const logs = [];
  const defaultLog = {
    EAPP_NO: "",
    BATCH_NO: "",
    POL_NO: "",
    BUNDLE_ID: "",
    SIGNATURE_DATE: 0,
    EXPIRY_DAY: 0, 
    CREATE_DATE: "",
    PREMIUM_IND: "",
    REMARK: ""
  };
  let agentCode = "";
  logger.log('INFO: signatureExpiry - start', cid, applicationId);

  return new Promise((resolve, reject) => {
    dao.getDocFromCacheFirst(sysParameter, param => {
      if (param && !param.error) {
        resolve(param);
      } else {
        reject(new Error("Fail to get sysParameter"));
      }
    })
  }).then(sysParameter => {
    return bundleDao.getCurrentBundle(cid).then(bundle => {
      if (!bundle) {
        throw new Error('Bundle is null');
      }
      agentCode = bundle.agentCode;
      return {
        bundle, 
        sysParameter
      };
    });
  }).then(({ sysParameter, bundle }) => {
    const { signExpireDay } = sysParameter;
    const expLimit = signExpireDay || 21;
    const expSignDate = moment().startOf("day").subtract(expLimit - 1, "days");

    // get signatureExpire View
    return new Promise(resolve => {
      dao.getViewRange(
        "main",
        "signatureExpire",
        `["01",0]`,
        `["01",${expSignDate.valueOf()}]`,
        null,
        resolve
      );
    }).then(result => {
      const { rows } = result;
      let appRows = [];
      let found = false;

      const filterRows = _.filter(rows, row => row.value.bundleId === bundle.id);
      if (applicationId) {
        appRows = _.filter(filterRows, row => row.value.appId === applicationId);
      } else {
        appRows = filterRows;
      }

      const appIds = _.map(appRows, row => row.value.appId);
      let appIdsString = "";

      _.forEach(bundle.applications, app => {
        const applicationDocId = _.get(app, "applicationDocId", "");
        const isFullySigned = _.get(app, "isFullySigned", false);

        const row = _.find(appRows, row => row.value.appId === applicationDocId);

        if (appIds.includes(applicationDocId)) {
          if (isFullySigned) {
            _.set(app, "appStatus", "INVALIDATED_SIGNED");
          } else {
            _.set(app, "appStatus", "INVALIDATED");
          }
          _.set(app, "invalidateReason", "Signature Expired");
          _.set(app, "invalidateDate", moment().valueOf());

          const newLog = _.cloneDeep(defaultLog);
          newLog.BATCH_NO = `${bundle.agentCode}_${now.toISOString()}`;
          newLog.EAPP_NO = applicationDocId;
          newLog.POL_NO = _.get(row, "value.polNo", "");
          newLog.BUNDLE_ID = bundle.id;
          newLog.SIGNATURE_DATE = _.get(row, "value.signDate", now.valueOf());
          newLog.EXPIRY_DAY = expLimit;
          newLog.CREATE_DATE = now.toISOString();
          newLog.PREMIUM_IND = `${_.get(row, "value.payMethod", "")}(${_.get(row, "value.payStatus", "")})`;

          logs.push(newLog);
          appIdsString += (applicationDocId + ";");

          found = true;
        }
      })

      if (found) {
        //update bundle
        return bundleDao.updateBundlePromise(bundle).then(res => {
          const bundleUpdResult = !!res ? "Update Success" : "Cannot save document";
          logger.log("INFO: signatureExpiry - Signature expired: "+ appIdsString + " on bundle = "+ bundle.id +  " : result " + bundleUpdResult);
          
          return _.map(logs, log => {
            log.REMARK += "Signature expired: Bundle " + bundleUpdResult + "." 
            return log;
          });
        }).then(logs => {
          //update applications
          const promises = _.map(appIds, appId => appDao.getApplicationPromise(appId));
          return Promise.all(promises).then(apps => {
            const upPromise = _.map(apps, app => {
              _.set(app, "isInvalidated", true);
              return appDao.upsertApplicationPromise(app.id, app);
            });
            return Promise.all(upPromise)
          }).then(upApps => {
            _.forEach(upApps, app => {
              const appUpdResult = !!app.error ? "Cannot save document" : "Update Success";
              const index = _.findIndex(logs, log => log.EAPP_NO === app.id);
              if (index > -1) {
                logs[index].REMARK += " Application " + appUpdResult + "."
              }
            });
            return logs;
          })
        });
      }
      return logs;

      // TODO: 
      // 1. handle Signed + Paid case in batch
      // 2. handle Signed + Paid case
    });
  }).then(logs => {
    if (logs.length > 0) {
      return addTrxLogs(agentCode, now, BATCH_SIGN_EXPIRY_TRX, logs);
    } else {
      return [];
    }
  }).then(result => {
    logger.log("INFO: signatureExpiry - end", cid, applicationId);
    cb({ success: true, logs: result });
  }).catch(error => {
    logger.error('ERROR: signatureExpiry - end', cid, error);
    const errorLogs = _.map(logs, log => {
      log.REMARK += ` > Exception: ${error.stack}`
      return log;
    });
    return addTrxLogs(agentCode, now, BATCH_SIGN_EXPIRY_TRX, errorLogs).then(result => {
      cb({ success: false, logs: result });
    }).catch(logError => {
      logger.error('ERROR: signatureExpiry - addTrxLogs', cid, elogErrorrror);
      cb({ success: false });
    });
  });
}

module.exports.signatureExpiry = signatureExpiry;