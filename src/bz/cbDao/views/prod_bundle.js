module.exports = {
    getBundleApps: function (doc, meta) {
        if (doc.type === 'bundle' && doc.applications && doc.applications.length) {
             var applications = [];
          for (var i in doc.applications) {
               var application = doc.applications[i];
            if (!application.invalidateDate) {
                 applications.push(application); 
            }
          }
          
          if (applications.length) {
            var signDateTime = (new Date(doc.fnaSignDate)).getTime();
                 emit(["01", signDateTime], {id: doc.id, data: applications});   
          }
        }
    },
    getProducts: function (doc, meta) {
        if (doc.type === 'quotation') {
          var plans = [];
          for (var i in doc.plans) {
           plans.push(doc.plans[i].covCode); 
          }
             emit(["01", doc.bundleId], {id: doc.id, data: plans});
        } else if (doc.type === 'application') {
          var plans = [];
          for (var i in doc.quotation.plans) {
           plans.push(doc.quotation.plans[i].covCode); 
          }
             emit(["01", doc.bundleId], {policyNumber: doc.policyNumber, id: doc.id, data: plans});
        }
    },
    missingEapprovalForm: function (doc, meta) {
         if (doc && ((doc.type === 'approval' && !doc.isShield) || doc.type === 'masterApproval')) {
              if (!doc._attachments || (doc._attachments && !doc._attachments.eapproval_supervisor_pdf)) {
                   if (
                        doc.accept && ['A','PFAFA', 'PDocFAF','PDisFAF'].indexOf(doc.approvalStatus) > -1 ||
                        doc.reject && ['R','PFAFA', 'PDocFAF','PDisFAF'].indexOf(doc.approvalStatus) > -1
                   ) {
                    emit(["01", doc._id], doc);
                   }
              }
         }
    }
}