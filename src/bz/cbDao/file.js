const dao = require('../cbDaoFactory').create();
const _ = require('lodash');

module.exports.getAttachment = function(docId, attId, callback) {

  dao.getAttachment(docId, attId, function(attachment) {
    callback(attachment);
	})
}

module.exports.getBinaryAttachment = function(docId, attId, callback) {
  dao.getBinaryAttachment(docId, attId, function(attachment) {
    callback(attachment);
  })
}

module.exports.getAttachmentByToken = function(token, callback) {
  if (token) {
    const docId = token.docId;
    const attId = token.attId;
    if (docId && attId) {
      dao.getDoc(docId, (doc) => {
        if (doc && !doc.error) {
          dao.getBinaryAttachment(docId, attId, (attachment) => {
            callback({
              contentType: _.get(doc, `_attachments.${attId}.content_type`, 'application/pdf'),
              data: attachment
            });
          });
        } else {
          callback({error: "invalid token"});
        }
      });
    } else {
      callback({error: "invalid token"});
    }
  } else {
    callback({error: "invalid token"});
  }
}

module.exports.getAttachmentContentType = function (docId, attId, callback) {
  dao.getDoc(docId, (doc) => {
    if (doc && doc._attachments) {
      var attachment = doc._attachments[attId];
      if (attachment) {
        callback(attachment.content_type);
        return;
      }
    }
    callback(false);
  });
};
