module.exports = {
  invalidationProducts: {
    'ASIM': {
      'baseProductIdList': ['08_product_ASIM_1', '08_product_ASIM_2', '08_product_ASIM_3'],
      'requoteInvalidMsgId': 'requoteInvalidDefaultMsg'
    },
    'NPE': {
      'baseProductIdList': ['08_product_NPE_1', '08_product_NPE_2'],
      'requoteInvalidMsgId': 'requoteInvalidDefaultMsg'
    }
  }
};