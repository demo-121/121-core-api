module.exports = {
    validBundleInClient: function (doc) {
        if (doc && doc.type === 'cust' && doc.bundle) {
            var validBundleId = '';
            for (var i in doc.bundle) {
                var bundle = doc.bundle[i];
                if (bundle.isValid) {
                    validBundleId = bundle.id;
                }
            }

            var emitObject = {
                id: doc.cid,
                validBundleId: validBundleId
            };
            emit(['01', doc.agentId], emitObject);
        }
    },

    quotationsByBundleId: function (doc) {
        if (doc && doc.type === 'quotation') {
            var emitObj = {
                id: doc.id,
                bundleId: doc.bundleId,
                baseProductCode: doc.baseProductCode,
                clientId: doc.pCid
            };
            emit(['01', doc.bundleId], emitObj);
        }
    },

    quotationsByBaseProductCode: function (doc) {
        if (doc && doc.type === 'quotation') {
            var emitObj = {
                bundleId: doc.bundleId,
                clientId: doc.pCid
            };
            emit(['01', doc.baseProductCode], emitObj);
        }
    },

    quotationsByMutipleBaseProductCode: function (doc) {
        if (doc && doc.type === 'quotation') {
            var emitObj = {
                bundleId: doc.bundleId,
                clientId: doc.pCid
            };
            if (doc.baseProductCode === 'ASIM' || doc.baseProductCode === 'NPE') {
                emit(['01', doc.baseProductCode], emitObj);
            }
        }
    },

    quotationsByNHAFFund: function (doc) {
        if (doc && doc.type === 'quotation') {
            var quotationCids = [];
            if (doc.pCid) {
                quotationCids.push(doc.pCid);
            }

            if (doc.iCid && doc.iCid !== doc.pCid) {
                quotationCids.push(doc.iCid);
            }
            var emitObj = {
                cids: quotationCids
            };
            var needEmit = false;
            if (doc.fund && doc.fund.funds && doc.fund.funds.length > 0) {
                for (var i = 0; i < doc.fund.funds.length; i++) {
                    var fundObj = doc.fund.funds[i];
                    if (fundObj && fundObj.fundCode === 'NHAF') {
                        needEmit = true;
                        break;
                    }
                }
            }
            if (needEmit) {
                emit(['01', doc.baseProductCode], emitObj);
            }
        }
    }
};