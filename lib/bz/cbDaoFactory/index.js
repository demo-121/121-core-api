var db = null;

export const create = () => {
  // the file is included from main project
  // e.g. /axa-sg-app/app/cbDaoFactory/index.js
  db = require('../../../../../app/cbDaoFactory');
  return db;
};

export default {
  create
};