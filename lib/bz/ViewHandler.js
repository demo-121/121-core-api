const dao = require('./cbDaoFactory').create();
module.exports.getViewnByParam = function (data, session, cb) {
    let result = [];
    return new Promise((resolve, reject) => {
        dao.getViewRange('main', data.view, data.startKey, data.endKey, null, function (pList) {
            if (pList && pList.rows) {
                for (var i = 0; i < pList.rows.length; i++) {
                    let valueObj = pList.rows[i].value;
                    valueObj.id = pList.rows[i].id;
                    result.push(valueObj);
                }
            }
            resolve(cb({ success: true, result }));
        });
    }).catch(e => {
        logger.error(e);
        cb({ success: false });
    });
};