var {
    callApi
} = require('./RemoteUtils.js');

// the file is included from main project
// e.g. /axa-sg-app/app/u
import axiosWrapper from '../../../../../app/utilities/axiosWrapper';

module.exports.sendEmail = function (email, cb, authToken, webServiceUrl) {

    // TODO: Keep the code
    // callApi('/email/sendEmail', email, cb)


    axiosWrapper({
        url: "/email/send",
        data: email,
        method: "POST",
        headers: {
            Authorization: `Bearer ${authToken}`
        },
        webServiceUrl
    }).then(cb).catch(e => {
        console.log(e);
    });
};