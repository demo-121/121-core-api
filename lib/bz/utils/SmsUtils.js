var {
    callApi
} = require('./RemoteUtils.js');

// the file is included from main project
// e.g. /axa-sg-app/app/u
import axiosWrapper from '../../../../../app/utilities/axiosWrapper';

module.exports.sendSms = function (sms, cb, authToken, webServiceUrl) {
    let newSmsObject = {
        mobileNumber: null,
        content: null
    };
    if (sms && sms.mobileNo) {
        newSmsObject.mobileNumber = sms.mobileNo;
    }
    if (sms && sms.smsMsg) {
        newSmsObject.content = sms.smsMsg;
    }

    if (newSmsObject.mobileNumber && newSmsObject.content) {
        axiosWrapper({
            url: "/sms/send",
            data: newSmsObject,
            method: "POST",
            headers: {
                Authorization: `Bearer ${authToken}`
            },
            webServiceUrl
        }).then(cb).catch(e => {
            console.log(e);
        });
    }
};